/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.servlet.api.listenerexample;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Attila
 */
@WebListener
public class HttpServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        sce.getServletContext().setAttribute("message", sce.getServletContext().getServerInfo());
        System.out.println("hu.braininghub.bh04.servlet.api.listenerexample.HttpServletContextListener.contextInitialized()");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.print("hu.braininghub.bh04.servlet.api.listenerexample.HttpServletContextListener.contextDestroyed()");
    }
    
}
