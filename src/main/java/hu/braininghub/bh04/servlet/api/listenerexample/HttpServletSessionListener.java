/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.servlet.api.listenerexample;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;

/**
 *
 * @author Attila
 */
@WebListener
public class HttpServletSessionListener  implements javax.servlet.http.HttpSessionListener{

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        
        se.getSession().setAttribute("code", 300);
        System.out.println("hu.braininghub.bh04.servlet.api.listenerexample.HttpServletSessionListener.sessionCreated()");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        System.out.println("hu.braininghub.bh04.servlet.api.listenerexample.HttpServletSessionListener.sessionDestroyed()");
      
    }
    
}
