/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.servlet.api.listenerexample;

import javax.servlet.ServletRequestEvent;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Attila
 */
@WebListener
public class HttpServletRequestListener implements javax.servlet.ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
       
       System.out.println("Servlet requestDestroyed => Address: "+sre.getServletRequest().getRemoteAddr()+" Host: "+sre.getServletRequest().getRemoteHost());
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println("Servlet requestInitialized => Address: "+sre.getServletRequest().getRemoteAddr()+" Host: "+sre.getServletRequest().getRemoteHost());
    }
    
}
