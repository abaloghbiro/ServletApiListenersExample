/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.servlet.api.listenerexample;

/**
 *
 * @author Attila
 */
public class A {
    
    private int value = 20;
    
    
    public A(){
        
    }
    
    public A(A other){
        other.value = 40;
    }
    
    
    public int getValue(){
        return value;
    }
    
    
    
    
    public static void main(String[]args){
        
        A a1 = new A();
        
        System.out.println(a1.getValue());
        A a2 = new A(a1);
        System.out.println(a1.getValue());
    }
}
